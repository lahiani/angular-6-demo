import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import {AuthService} from './services/auth.service';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ScrollEventModule } from 'ngx-scroll-event';
import {HttpModule} from '@angular/http';

const tabRoute: Routes = [
  { path: '', component: LoginComponent},
  { path: 'home', component: HomeComponent}];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollEventModule,
    HttpModule,
    RouterModule.forRoot(tabRoute)
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
