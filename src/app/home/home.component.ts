import {Component, OnInit} from '@angular/core';
import {ScrollEvent} from 'ngx-scroll-event';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  etat = false;
  loading = true;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      console.log(`the user is reaching the bottom`);
      this.etat = !this.etat;
    }
  }

  back() {
    this.router.navigate(['/']);
  }
}
