import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: Http) {
  }

  public addUser(user: User) {
    return this.http.post(this.url, JSON.stringify(user));
  }
}
