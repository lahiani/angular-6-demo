import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../models/User';
import {AuthService} from '../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private ls: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.createUser(this.f.username.value, this.f.password.value);
    if (this.f.username.value === 'admin') {
      this.router.navigate(['/home']);
    } else {
      this.loading = false;
    }
  }

  createUser(username, password) {
    const user = new User;
    user.username = username;
    user.password = password;
    this.ls.addUser(user)
      .subscribe(response => {
        console.log(response.json());
      });
  }

}
